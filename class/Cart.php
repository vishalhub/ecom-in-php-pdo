<?php

require_once 'Crud.php';

class Cart extends Crud {
    // this is for add your product to cart
    public function add_to_cart($product_id, $quantity){
        
        // check your user is logged in or not
        if(!isset($_SESSION['loggedIn'])){
            return $output = [
                'status' => false,
                'message' => 'You are not logged in, please login.'
            ];
        } 
        else {
            // if user is logged in
            $user_id = $_SESSION['id'];

            $checkCart = $this->custom_get('cart', " WHERE product_id = $product_id AND user_id = $user_id", 'fetch');

            if($checkCart){
                // if i already have the same product in my cart
                $cart_id = $checkCart['id'];
                $cartItem = [
                    'quantity' => $checkCart['quantity'] + 1
                ];

                $update_cart =  $this->update('cart', $cartItem, " WHERE id = '$cart_id' ");

                if($update_cart){
                    return $output = [
                        'status' => true,
                        'message' => 'Product has been updated'
                    ];
                }


            } else {
                // store my product into database
                $cartItem = [
                    'product_id' => $product_id,
                    'user_id' => $user_id,
                    'quantity' => $quantity
                ];

                $insertIntoCart = $this->insert('cart', $cartItem);
                if($insertIntoCart){
                    return [
                        'status' => true,
                        'message' => 'Product has been added successfully'
                    ];
                } else {
                    return [
                        'status' => false,
                        'message' => 'Something went worng'
                    ];
                }
            }

        }
    }

    public function update_cart($cart_id, $quantity){
        // store my product into database
        $cartItem = [
            'quantity' => $quantity
        ];

        $updateCart = $this->update('cart', $cartItem, " WHERE id = '$cart_id' ");
        if($updateCart){
            return [
                'status' => true,
                'message' => 'Product has been updated successfully'
            ];
        }
    }

    // remove my cart items from database
    public function delete_cart($cart_id){
        if($this->delete("cart", " WHERE id = '$cart_id'")){
            return [
                'status' => true,
                'message' => 'Product has been removed successfully'
            ];
        } else {
            return [
                'status' => false,
                'message' => 'Something went wrong'
            ];
        }
    }
}