<?php

require_once '../../class/crud.php';

$obj = new Crud();

if($_POST['action'] == 'edit'){
    $cat_id = $_POST['cat_id'];
    $a = $obj->custom_get('category',"WHERE `category_id` = '$cat_id'");

    foreach($a as $row){
        echo json_encode($row);
    }

}
if($_POST['action'] == 'delete'){
    $cat_id = $_POST['id'];
    $q = $obj->delete('category',"WHERE `category_id` = '$cat_id'");
    if($q){
        $data = [
            'status' => 200,
            'message' => 'Category has been deleted',
        ];
    } else{
        $data = [
            'status' => 203,
            'message' => 'Something went wrong',
        ];
    }

    echo json_encode($data);
}

if($_POST['action'] == 'fetch_brand'){
    $cat_id = $_POST['cat_id'];

    $query = $obj->custom_get("brand", "WHERE `brand_category_id` = '$cat_id'");
    $output = '<option selected disabled>--Select your brand name --</option>';
    foreach($query as $brand){
        $output .= '<option value="'.$brand['brand_id'].'">'.$brand['brand_name'].'</option>';
    }
    echo $output;

}
?>