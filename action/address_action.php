<?php
session_start();

require_once '../class/crud.php';

$obj = new Crud();


if($_SERVER['REQUEST_METHOD'] == 'POST'){

    // Validate user session
    if (!isset($_SESSION['id'])) {
        echo json_encode(['success' => false, 'message' => 'User not logged in.']);
        exit;
    }
    $first_name = htmlspecialchars(trim($_POST['first_name']));
    $last_name = htmlspecialchars(trim($_POST['last_name']));
    $phone = htmlspecialchars(trim($_POST['phone']));
    $email = htmlspecialchars(trim($_POST['email']));
    $address_1 = htmlspecialchars(trim($_POST['address_1']));
    $address_2 = htmlspecialchars(trim($_POST['address_2']));
    $city = htmlspecialchars(trim($_POST['city']));
    $pincode = htmlspecialchars(trim($_POST['pincode']));
    $state = htmlspecialchars(trim($_POST['state']));
    $country = htmlspecialchars(trim($_POST['country']));
    $user_id = $_SESSION['id'];
    $created_at = date("Y-m-d H:i:s");

    // Basic validation
    if (empty($first_name) || empty($last_name) || empty($phone) || empty($email) || empty($address_1) || empty($city) || empty($pincode) || empty($state) || empty($country)) {
        echo json_encode(['success' => false, 'message' => 'All required fields must be filled.']);
        exit;
    }

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        echo json_encode(['success' => false, 'message' => 'Invalid email address.']);
        exit;
    }

    if (!preg_match('/^\d{10}$/', $phone)) {
        echo json_encode(['success' => false, 'message' => 'Invalid phone number.']);
        exit;
    }

    if (!preg_match('/^\d{5,6}$/', $pincode)) {
        echo json_encode(['success' => false, 'message' => 'Invalid pincode.']);
        exit;
    }

    $insert_data = [
        'first_name' => $first_name,
        'last_name' => $last_name,
        'phone_number' => $phone,
        'email' => $email,
        'address_one' => $address_1,
        'address_two' => $address_2,
        'city_name' => $city,
        'pincode' => $pincode,
        'state_name' => $state,
        'country_name' => $country,
        'user_id' => $user_id,
        'created_at' => $created_at
    ];

    $query = $obj->insert('addresses', $insert_data);

    // if our address data is inserted
    // Respond based on the result
    if ($query) {
        echo json_encode(['success' => true, 'message' => 'Address added successfully.']);
    } else {
        echo json_encode(['success' => false, 'message' => 'Something went wrong while adding the address.']);
    }

}