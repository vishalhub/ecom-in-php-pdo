$(document).ready(function(){
    $('.menu-btn').click(function(){
        $('.sidebar').css({
            'width': '70px',
            'font-size': '35px',
            'margin-top': '-10px'
        });
        $('.text-link').hide();
        $('.close-btn').show();
        $('.menu-btn').hide();
    });
    
    $('.close-btn').click(function(){
        $('.sidebar').css({
            'width' : '300px',
            'font-size': '16px'
        });
        $('.text-link').show();
        $('.close-btn').hide();
        $('.menu-btn').show();
    });


    $('.add-category-btn').click(function(){
        alert('worked');
    })

    $('.add_category').click(function(){
        $('#catModal').modal('show');
        $('.modal-title').text('Add a New Category');
        $('#form_type').val('save');
    });

    $('.add_brand').click(function(){
        $('#brandModal').modal('show');
        $('.modal-title').text('Add a New Brand');
        $('#form_type').val('save');
    });

    // this is for product modal
    $('.add_product').click(function(){
        $('#submit').text("Save")
        $('#product_form')[0].reset();
        $('#productModal').modal('show');
        $('.modal-title').text('Add a New Product');
        $('#form_type').val('save');
        $(".toggle_product_thumbnail").hide();
        $('#fetched_product_thumbnail').attr('src', null);
    });

    var count = 0;
    $(".add-more-thumbnail").click(function(){
        count = count + 1;
        var html = '<div class="row" id="row-'+count+'">';
        html += '<div class="col-md-12">';
        html += '    <label>Add More Thumbnails</label>';
        html += '</div>';
        html += '<div class="col-md-10">';
        html += '    <input type="file" class="form-control">';
        html += '</div>';
        html += '<div class="col-md-2">';
        html += '    <button type="button" id="'+count+'" class="btn btn-danger btn-block remove">Remove</button>';
        html += '</div>';
        html += '</div>';

        $(".extra-thumbnail-area").append(html);
    });

    $(document).on('click','.remove',function(){
        var row_data = $(this).attr('id');
        $("#row-"+row_data).remove();
    });
    
});
