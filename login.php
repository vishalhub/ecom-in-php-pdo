<?php
session_start();
if(isset($_SESSION['loggedIn'])){
    header('Location:index.php');
}


// include required things
include 'includes/header.php';
include 'includes/navbar.php';
?>
<!-- product detail page -->
<div class="card login-area">
    <div class="card-body">
        <div class="container">
            <h1 class="text-center py-5">Login</h1>

            <div class="border p-4 login-card">
                <form id="loginForm" action="user_registration_action.php" method="post">
                    <div class="row">
                        

                        <!-- Email -->
                        <div class="col-md-12 mb-3">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" id="email" name="email" placeholder="Email"
                                    class="form-control py-3">
                                    <div class="text-danger" id="email_error"></div>
                            </div>
                        </div>

                        

                        <!-- Password -->
                        <div class="col-md-12 mb-3">
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" id="password" name="password" placeholder="Please Enter Password"
                                    class="form-control py-3">
                                    <div class="text-danger" id="password_error"></div>
                            </div>
                        </div>

                        <div class="col-md-12 d-flex justify-content-center">
                            <button type="submit" class="btn btn-primary btn-lg">Login</button>
                        </div>

                        <div>
                            <p class="text-center mt-4">Don't have an Account ? <a href="register.php">Create Account.</a></p>
                        </div>
                        <div class="col-md-12" id="message"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



<?php include 'includes/footer.php'; ?>
<script>
    
    
    $(document).ready(function() {
        // create function for user registration
        $("#loginForm").on("submit", function(event) {

            event.preventDefault();

            // clear all previous errors
            $('.text-danger').html("");

            // front end validation
            const fields = {
                email : $("#email").val(),
                password : $("#password").val()
            }
            // console.log(fields);
            let valid = true;

            const errors = {}; //initilize errors variable for store your error messages

            if(fields.email.trim() === ""){
                valid = false;
                errors.email_error = "Email is required";
            }
            if(fields.password.trim() === ""){
                valid = false;
                errors.password_error = "Password field is required";
            }
            // display errors
            if(!valid){
                // console.log(errors)
                for(const [key, value] of Object.entries(errors)){
                    $("#"+key).html(value);
                }
                return;
            }
            let fd = $("#loginForm").serialize();
            // create ajax request
            $.ajax({
                url: "action/user_login_action.php",
                type: "POST",
                data: fd,
                dataType: "json",
                success: function(response){
                    // console.log(response);
                    if(response.status == true){
                        // $("#message").html('<div class= "alert alert-success">'+ response.message+ '</div>');

                        const toastLiveExample = document.getElementById('liveToast')
                        const toastBootstrap = bootstrap.Toast.getOrCreateInstance(toastLiveExample)
                        $('.toast-message').text(response.message)
                        toastBootstrap.show();
                        $("#loginForm")[0].reset();

                        setTimeout(() => {
                            window.location.href = 'index.php';
                        }, 1000);
                    }
                    if(response.status == false){
                        $.each(response.errors, function(key, value) {
                            $("#"+key).html(value);
                        })
                    }
                }
            })

            

        });
    });
</script>