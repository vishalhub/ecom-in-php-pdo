<?php
// include required things
include 'includes/header.php';
include 'includes/navbar.php';
?>
<!-- product detail page -->
<div class="card">
    <div class="card-body">
        <div class="container">
            <h1 class="text-center py-5">Registration</h1>

            <div class="border p-4">
                <form id="registrationForm" action="user_registration_action.php" method="post">
                    <div class="row">
                        <!-- first name -->
                        <div class="col-md-6 mb-3">
                            <div class="form-group">
                                <label for="first_name">First Name</label>
                                <input type="text" id="first_name" name="first_name" placeholder="First Name"
                                    class="form-control py-3">

                                <div class="text-danger" id="first_name_error"></div>
                            </div>
                        </div>

                        <!-- last name -->
                        <div class="col-md-6 mb-3">
                            <div class="form-group">
                                <label for="last_name">Last Name</label>
                                <input type="text" id="last_name" name="last_name" placeholder="Last Name"
                                    class="form-control py-3">

                                    <div class="text-danger" id="last_name_error"></div>
                            </div>
                        </div>

                        <!-- Email -->
                        <div class="col-md-6 mb-3">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" id="email" name="email" placeholder="Email"
                                    class="form-control py-3">
                                    <div class="text-danger" id="email_error"></div>
                            </div>
                        </div>

                        <!-- Mobile Number -->
                        <div class="col-md-6 mb-3">
                            <div class="form-group">
                                <label for="mobile">Mobile No.</label>
                                <input type="tel" id="mobile" name="mobile" placeholder="Mobile No."
                                    class="form-control py-3">

                                    <div class="text-danger" id="mobile_error"></div>
                            </div>
                        </div>

                        <!-- Password -->
                        <div class="col-md-6 mb-3">
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" id="password" name="password" placeholder="Please Enter Password"
                                    class="form-control py-3">
                                    <div class="text-danger" id="password_error"></div>
                            </div>
                        </div>

                        <!-- Confirm Password -->
                        <div class="col-md-6 mb-3">
                            <div class="form-group">
                                <label for="confirm_password">Confirm Password</label>
                                <input type="password" id="confirm_password" name="confirm_password"
                                    placeholder="Please Confirm your password" class="form-control py-3">

                                    <div class="text-danger" id="confirm_password_error"></div>
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <input type="checkbox" id="terms">
                            <label for="terms">Agree with user's terms & conditions.</label>
                            <div class="text-danger" id="terms_error"></div>
                        </div>

                        <div class="col-md-12 d-flex justify-content-center">
                            <button type="submit" class="btn btn-primary btn-lg">Register</button>
                            <button type="reset" class="btn btn-warning btn-lg ms-4 px-4">Reset</button>

                            
                        </div>
                        <div>
                            <p class="text-center mt-4">Already have an Account ? <a href="login.php">Click here for login</a></p>
                        </div>
                        <div class="col-md-12" id="message"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php include 'includes/footer.php'; ?>
<script>
    $(document).ready(function() {
        // create function for user registration
        $("#registrationForm").on("submit", function(event) {

            event.preventDefault();

            // clear all previous errors
            $('.text-danger').html("");

            // front end validation
            const fields = {
                first_name : $("#first_name").val(),
                last_name : $("#last_name").val(),
                email : $("#email").val(),
                mobile : $("#mobile").val(),
                password : $("#password").val(),
                confirm_password : $("#confirm_password").val(),
                terms : $("#terms")
            }
            // console.log(fields);
            let valid = true;

            const errors = {}; //initilize errors variable for store your error messages

            if(fields.first_name.trim() === ""){
                valid = false;
                errors.first_name_error = "First name is required";
            }

            if(fields.last_name.trim() === ""){
                valid = false;
                errors.last_name_error = "Last name is required";
            }

            if(fields.email.trim() === ""){
                valid = false;
                errors.email_error = "Email is required";
            }

            if(fields.mobile.trim() === ""){
                valid = false;
                errors.mobile_error = "Mobile Number is required";
            }
            if(fields.password.trim() === ""){
                valid = false;
                errors.password_error = "Password field is required";
            }
            if(fields.confirm_password.trim() === ""){
                valid = false;
                errors.confirm_password_error = "Confirm password field is required";
            } else if(fields.confirm_password !== fields.password){
                valid = false;
                errors.confirm_password_error = "Password do not match.";
            }

            if(!fields.terms.is(":checked")) {
                valid = false;
                errors.terms_error = "You must agree with terms and conditions.";
            }

            // display errors
            if(!valid){
                // console.log(errors)
                for(const [key, value] of Object.entries(errors)){
                    $("#"+key).html(value);
                }
                return;
            }
            let fd = $("#registrationForm").serialize();
            // create ajax request
            $.ajax({
                url: "action/user_registration_action.php",
                type: "POST",
                data: fd,
                dataType: "json",
                success: function(response){
                    // console.log(response);
                    if(response.status == 'success'){
                        $("#message").html('<div class= "alert alert-success">'+ response.message+ '</div>');
                        $("#registrationForm")[0].reset();
                    }
                    if(response.status == false){
                        $.each(response.errors, function(key, value) {
                            $("#"+key).html(value);
                        })
                    }
                }
            })

            

        });
    });
</script>