<?php
require_once '../../class/Crud.php';

$obj = new Crud();
if($_POST['form_type'] == 'save'){
    if(empty($_POST['category_name'])){
        $data['msg_error'] = 'Please Fill the Category Field';
        $data['status'] = 0;
    } else{
        $data = [
            'category_name' => $_POST['category_name'],
            'category_slug_url' => $obj->slugify($_POST['category_name'],'category_slug_url','category'),
        ];

        if($obj->insert('category', $data)){
           $data['status'] = 1;
        } else{
            $data['status'] = 0;
        }
    }

    echo json_encode($data);
}

if($_POST['form_type'] == 'edit'){
    // echo 'update'; die();
    if(empty($_POST['category_name'])){
        $data['msg_error'] = 'Please Fill the Category Field';
        $data['status'] = 0;
    } else{
        $data = [
            'category_name' => $_POST['category_name'],
            'category_slug_url' => $obj->slugify($_POST['category_name'],'category_slug_url','category'),
        ];
        $id = $_POST['id'];
        if($obj->update('category', $data," WHERE `category_id` = $id")){
           $data['status'] = 1;
        } else{
            $data['status'] = 0;
        }
    }

    echo json_encode($data);
}
?>