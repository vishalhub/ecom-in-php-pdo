<div class="navigation-bar">
            <div class="container">
                <!-- insert a logo -->
                <div class="logo">
                    <a href="index.php">
                        <img src="images/logo.png" alt="logo">
                    </a>
                </div>

                <!-- search box -->
                <div class="search-area">
                    <form action="" method="post">
                        <input type="text" name="search_box" class="search_box" placeholder="Search all items...">
                        <button class="search_btn btn btn-success"><i class="fas fa-search"></i></button>
                    </form>
                </div>

                <!-- user menu -->
                <div class="user-menu">
                    <ul>
                        <li class="dropdown-center">
                            <a href="#" class="dropdown-toggle" data-bs-toggle="dropdown" aria-hoshpopup="true"
                                aria-expanded="false"><i class="fas fa-user"></i> My Account</a>


                            <!-- dropdown box -->
                            <div class="dropdown-menu dropdown-menu-right bg-dark" style="width:250px;">
                            <?php 
                            session_start();
                            if(isset($_SESSION['loggedIn'])){
                                // if you are logged in
                                ?>
                                <a href="#"><button type="button" class="dropdown-item"><i
                                            class="fas fa-user"></i>&nbsp;Your Account</button></a>
                                <a href="#"><button type="button" class="dropdown-item"><i
                                            class="fas fa-cube"></i>&nbsp;Your Order</button></a>
                                <a href="#"><button type="button" class="dropdown-item"><i
                                            class="fas fa-heart"></i>&nbsp;Wishlist</button></a>

                                <a href="logout.php"><button type="button" class="dropdown-item"><i
                                            class="fas fa-sign-out-alt"></i>&nbsp;Logout</button></a>
                                <?php } 
                                 
                                 else { 
                                   //if not logged in 
                                    ?>
                                <div class="dropdown-divider"></div>
                                <p class="text-center text-white" style="height:15px; line-height:20px;"><small>if you
                                        are a new user</small></p>
                                <a href="register.php"><button type="button" class="dropdown-item text-center"><i
                                            class="fas fa-user"></i>&nbsp;Register</button></a>
                                <a href="login.php"><button type="button" class="dropdown-item text-center bg-danger"><i
                                            class="fas fa-user"></i>&nbsp;LOGIN</button></a>
                                <?php  } ?>
                            </div>

                        </li>
                        <li><a href="cart.php"><i class="fas fa-shopping-cart"></i> cart</a></li>
                    </ul>
                </div>
            </div>
        </div>