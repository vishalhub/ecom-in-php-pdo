<?php
include 'includes/header.php';
include 'includes/navbar.php';
?>


<div class="slider-area">
    <div class="slider">
        <div>
            <a href="#">
                <img src="images/slider/slider-1.jpg" alt="">
            </a>
            <!-- slider content -->
            <div class="slider-content">
                <h3 class="text-white text-capitalize">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Provident, dolor.</h3>
                <a href="#"><button class="btn btn-primary"><i class="fas fa-shopping-cart"></i> Buy
                        Now</button></a>
                <a href="#"><button class="btn btn-outline-danger ml-5">Read More</button></a>
            </div>
        </div>
        <div>
            <a href="#">
                <img src="images/slider/slider-2.jpg" alt="">
            </a>
            <!-- slider content -->
            <div class="slider-content">
                <h3 class="text-white text-capitalize">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Provident, dolor.</h3>
                <a href="#"><button class="btn btn-primary"><i class="fas fa-shopping-cart"></i> Buy
                        Now</button></a>
                <a href="#"><button class="btn btn-outline-danger ml-5">Read More</button></a>
            </div>
        </div>
        <div>
            <a href="#">
                <img src="images/slider/slider-3.jpg" alt="">
            </a>
            <!-- slider content -->
            <div class="slider-content">
                <h3 class="text-white text-capitalize">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Provident, dolor.</h3>
                <a href="#"><button class="btn btn-primary"><i class="fas fa-shopping-cart"></i> Buy
                        Now</button></a>
                <a href="#"><button class="btn btn-outline-danger ml-5">Read More</button></a>
            </div>
        </div>
    </div>
</div>

<!-- this is for work area -->
<div class="container-fluid">
    <!-- product section -->
    <section class="product-section">
        <div class="section-heading">
            <h3 class="heading">latest Products</h3>
        </div>


        <div class="section-product-cards">

            <div class="owl-carousel">
                <?php
                $fetch_latest_products = $obj->custom_get("products", " WHERE status = '1' ORDER BY product_id DESC");

                foreach ($fetch_latest_products as $latest_product):
                    ?>
                    <div class="product-card">
                        <div class="product-image">
                            <img src="uploads/products/<?php echo $latest_product['product_thumbnail']; ?>"
                                alt="product name">
                        </div>

                        <!-- product hovered contents -->
                        <div class="card-contents">
                            <button type="button" class="btn btn-warning cart-btn product-add-cart-btn" data-product-id="<?php echo $latest_product['product_id']; ?>">
                                <i class="fas fa-cart-plus"></i>
                            </button>
                        </div>

                        <a href="product.php?product_id=<?php echo $latest_product['product_id']; ?>">
                            <div class="product-details">
                                <!-- product name -->
                                <h5 class="product-name"><?php echo $latest_product['product_title']; ?></h5>
                                <p class="product-price">
                                    <small
                                        class="text-danger"><s><?php echo $latest_product['regular_price']; ?></s></small>
                                    <span class="text-success"><?php echo $latest_product['selling_price']; ?></span>
                                </p>
                            </div>
                        </a>
                    </div>

                <?php endforeach; ?>


            </div>
        </div>
    </section>
</div>
</div>
<?php include 'includes/footer.php'; ?>