<?php
// include required things
include 'includes/header.php';
include 'includes/navbar.php';

$product_id = $_GET['product_id'];

$fetch_product = $obj->custom_get('products'," LEFT JOIN `category` ON `category`.`category_id` = `products`.`category_id` WHERE product_id = $product_id", 'fetch');

$regular_price = $fetch_product['regular_price'];
$selling_price = $fetch_product['selling_price'];

$price_diffrance = $regular_price - $selling_price;

$discount_in_percantage = ($price_diffrance /$regular_price ) * 100;
// echo '<pre>';print_r($fetch_product); 
// die();
?>
<!-- product detail page -->
<div class="card">
    <div class="card-body">
        <div class="container">
            <div class="row product-custom-row">
                <div class="col-md-6">
                    <div class="product_details_image">
                        <img src="uploads/products/<?php echo $fetch_product['product_thumbnail']; ?>" class="img-fluid">
                    </div>
                </div>

                <div class="col-md-6">
                    <span class="badge bg-dark p-2"><?php echo $fetch_product['category_name']; ?></span>

                    <div class="product-detail-title">
                        <h1><?php echo $fetch_product['product_title']; ?></h1>
                    </div>

                    <div class="product-detail-short-description">
                        <p><?php echo $fetch_product['short_description']; ?></p>
                    </div>
                    <div class="product-detail-price">
                        <span class="font-weight-bold">-<?php echo round($discount_in_percantage, 0); ?>%</span>
                        <span class="selling-price">$<?php echo $fetch_product['selling_price']; ?></span>
                        <span class="regular-price"><del class="text-danger">$<?php echo $fetch_product['regular_price']; ?></del></span>
                    </div>

                    <button type="button" class="btn btn-dark btn-lg product-add-cart-btn" data-product-id="<?php echo $fetch_product['product_id']; ?>">
                        <i class="fa fa-cart-plus"></i> Add to Cart</button>
                </div>

                <div class="col-md-12 mt-5">
                    <h2>Description</h2>
                    <hr>

                    <p class="product-detail-long-description"><?php echo $fetch_product['long_description']; ?></p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'includes/footer.php'; ?>
