<?php
// include required things
include 'includes/header.php';
include 'includes/navbar.php';

require_once 'class/Crud.php';

$obj = new Crud();

?>
<div class="card">
    <div class="card-body">
        <h1 class="text-center py-5">Manage All Address</h1>

        <!-- container start -->
        <div class="container">
            <?php
            if (!isset($_SESSION['loggedIn'])) { ?>

                <h1 class="text-center pt-5 pb-3">You are not logged In.</h1>
                <div class="d-flex justify-content-center">
                    <a href="login.php" class="nav-link">Click here for login</a>
                </div>

            <?php } else {

                if (!empty($_GET['message']) && $_GET['message'] == 'success') {
                    echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Your address has been saved in your account successfuly.</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>';
                }
                $user_id = $_SESSION['id'];

                // fetch all address
                // $all_address = $obj->custom_get('addresses', " WHERE user_id = $user_id", "fetchAll");
            
                // echo '<pre>';print_r($all_address); die();
            
                ?>
                <div class="row">
                    <div class="col-md-7">
                        <!-- User billing address area -->
                        <h1 class="border-bottom pb-3 mb-4">Add new billing Address</h1>
                        <form action="" method="post" id="address_form">
                            <div class="row">
                                <div class="form-group mb-3 col-md-6">
                                    <label for="first_name">First Name</label>
                                    <input type="text" name="first_name" id="first_name" placeholder="First Name"
                                        class="form-control" reuqired>
                                </div>

                                <div class="form-group mb-3 col-md-6">
                                    <label for="last_name">Last Name</label>
                                    <input type="text" name="last_name" id="last_name" placeholder="Last Name"
                                        class="form-control">
                                </div>

                                <div class="form-group mb-3 col-md-6">
                                    <label for="phone">Phone Number</label>
                                    <input type="tel" name="phone" id="phone" placeholder="Phone Number"
                                        class="form-control">
                                </div>

                                <div class="form-group mb-3 col-md-6">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" id="email" placeholder="Email" class="form-control">
                                </div>

                                <!-- User address 1 -->
                                <div class="form-group mb-3 col-md-12">
                                    <label for="address_1">Address 1</label>
                                    <input type="text" name="address_1" id="address_1" placeholder="Address 1"
                                        class="form-control">
                                </div>

                                <!-- User address 2 -->
                                <div class="form-group mb-3 col-md-12">
                                    <label for="address_1">Address 2</label>
                                    <input type="text" name="address_2" id="address_2" placeholder="Address 2 (Optional)"
                                        class="form-control">
                                </div>

                                <!-- City Name -->
                                <div class="form-group mb-3 col-md-6">
                                    <label for="city">City Name (i.e New york)</label>
                                    <input type="text" name="city" id="city" placeholder="City Name (i.e New york)"
                                        class="form-control">
                                </div>

                                <!-- City Name -->
                                <div class="form-group mb-3 col-md-6">
                                    <label for="pincode">Pincode</label>
                                    <input type="text" name="pincode" id="pincode" placeholder="Enter your Pincode"
                                        class="form-control">
                                </div>

                                <!-- State -->
                                <div class="form-group mb-3 col-md-6">
                                    <label for="state">State Name</label>
                                    <input type="text" name="state" id="state" placeholder="State Name"
                                        class="form-control">
                                </div>

                                <!-- Country -->
                                <div class="form-group mb-3 col-md-6">
                                    <label for="country">Country Name</label>

                                    <select name="country" id="country" class="form-select">
                                        <option selected disabled>--Select your Country--</option>
                                        <option value="India">India</option>
                                        <option value="USA">USA</option>
                                        <option value="Canada">Canada</option>
                                        <option value="Nepal">Nepal</option>
                                    </select>
                                </div>

                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-success">Add Address</button>
                                </div>

                            </div>
                        </form>
                    </div>

                    <div class="col-md-5 p-3" style="background: #f5f5f5;">
                        <!-- added cart items information -->
                        <h3>Select Address</h3>
                        <hr>
                        <ul class="list-group list-group-flush">
                            <!-- Address list content will be dynamically loaded here -->
                        </ul>
                        <button class="btn btn-success btn-lg w-100">Select</button>

                    </div>
                </div>
            </div>

        <?php } ?>
    </div>
</div>

<?php
include 'includes/footer.php';
?>

<script>
    $(document).ready(function () {
        function reloadAddressSection() {
            $.ajax({
                url: "/action/render_address.php", // Update with your endpoint
                method: "GET",
                success: function (html) {
                    $('.list-group').html(html); // Replace the address list content
                },
                error: function (xhr, status, error) {
                    console.error("Error reloading address section:", error);
                    alert("Failed to reload address section.");
                }
            });
        }

        // Initial load of the address section
        reloadAddressSection();



        $('#address_form').on("submit", function (event) {
            event.preventDefault();

            let isValid = true;

            // tthis is for only checking the required things
            $('#address_form input, #address_form select').each(function () {
                let $this = $(this); // Cache the current element
                if ($this.val().trim() === '') {
                    isValid = false;
                    $this.addClass('is-invalid'); // Add invalid class
                } else {
                    $this.removeClass('is-invalid'); // Remove invalid class
                }
            });

            // Helper function for individual validations
            function validateField(selector, pattern) {
                const $field = $(selector);
                if (!pattern.test($field.val())) {
                    isValid = false;
                    $field.addClass('is-invalid');
                } else {
                    $field.removeClass('is-invalid');
                }
            }

            // Email validation
            const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
            validateField('#email', emailPattern);

            // Phone validation (10 digits)
            const phonePattern = /^\d{10}$/;
            validateField('#phone', phonePattern);

            // Pincode validation (5-6 digits)
            const pincodePattern = /^\d{5,6}$/;
            validateField('#pincode', pincodePattern);

            // If validation fails, stop form submission
            if (!isValid) {
                return;
            }

            const formData = $(this).serialize();


            // send data via AJAX request
            $.ajax({
                url: "/action/address_action.php", // Replace with your server URL
                method: "POST",
                data: formData,
                dataType: "json",
                success: function (response) {
                    if (response.success) {

                        const toastLiveExample = document.getElementById('liveToast')
                        const toastBootstrap = bootstrap.Toast.getOrCreateInstance(toastLiveExample)
                        $('.toast-message').text(response.message)
                        toastBootstrap.show();

                        $('#address_form')[0].reset(); // Reset form
                        reloadAddressSection(); // Reload the address list section
                    } else {
                        alert("Error: " + response.message);
                    }
                },
                error: function (xhr, status, error) {
                    console.error("AJAX Error:", error);
                    alert("An error occurred while submitting the form.");
                }
            });
        });
    });
</script>