<?php
require_once '../../class/crud.php';
$obj = new Crud();

if ($_POST['form_type'] == 'save') {
    if (empty($_POST['category_name'])) {
        $data['msg_error'] = 'Please select a category';
        $data['status'] = 0;
    }

    if (empty($_POST['brand_name'])) {
        $data['msg_error'] = 'Please Fill the Brand Field';
        $data['status'] = 0;
    } else {
        $data = [
            'brand_name' => $_POST['brand_name'],
            'brand_slug_name' => $obj->slugify($_POST['brand_name'], 'brand_slug_name', 'brand'),
            'brand_category_id' => $_POST['category_name'],
            'brand_created_at' => date('Y-m-d H:i:s')
        ];

        if ($obj->insert('brand', $data)) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }
    }
    echo json_encode($data);
}

if ($_POST['form_type'] == 'edit') {
    $brand_id = $_POST['brand_id'];
    $query = $obj->custom_get("brand", "WHERE brand_id = '$brand_id'");

    echo json_encode($query);
}

if ($_POST['form_type'] == 'update_brand') {
    $brand_id = $_POST['brand_id'];
    $data = [
        'brand_name' => $_POST['brand_name'],
        'brand_slug_name' => $obj->slugify($_POST['brand_name'], 'brand_slug_name', 'brand'),
        'brand_category_id' => $_POST['category_name'],
        'brand_created_at' => date('Y-m-d H:i:s')
    ];

    $update_query = $obj->update("brand", $data, "WHERE brand_id = '$brand_id'");

    if ($update_query) {
        $output = [
            'status' => '1',
            'message' => "brand data succesfully updated"
        ];

        echo json_encode($output);
    }
}
if ($_POST['form_type'] == 'delete_brand') {
    $brand_id = $_POST['brand_id'];
    $delete_product = $obj->delete("brand", " where brand_id = '$brand_id'");

    if ($delete_product) {
        $result = [
            'status' => 200,
            'message' => "Brand deleted successfully"
        ];
    } else {
        $result = [
            'status' => 401,
            'message' => "Something went wrong"
        ];
    }

    echo json_encode($result);
}

?>