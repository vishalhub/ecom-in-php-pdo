$(document).ready(function(){
    $('.slider').bxSlider({
        auto:true,
    });
    
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        responsiveClass:true,
        responsive:{
            0: {
                items:1,
                nav:true
            },
            600:{
                items:3,
                nav:false
            },
            1000:{
                items:4,
                nav:true,
                loop:false
            },
            1300:{
                items:5,
                nav:true,
                loop:false
            }
        }
    });

    // this is for add our product to cart
    $('.product-add-cart-btn').click(function(){
        let product_id  = $(this).data('product-id');
        $.ajax({
            url: "action/add-to-cart.php",
            type: "POST",
            data: {
                product_id:product_id
            },
            dataType: "json",
            success: function(response){
                if(response.status == false){
                    window.location.href = 'login.php';
                }

                if(response.status == true){
                    console.log(response)
                    const toastLiveExample = document.getElementById('liveToast')
                        const toastBootstrap = bootstrap.Toast.getOrCreateInstance(toastLiveExample)
                        $('.toast-message').text(response.message)
                        toastBootstrap.show();
                }
            }
        })
    });
});