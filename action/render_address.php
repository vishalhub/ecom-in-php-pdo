<?php
session_start();

require_once '../class/crud.php';

$obj = new Crud();

// Validate user session
if (!isset($_SESSION['id'])) {
    echo '<li class="list-group-item">User not logged in.</li>';
    exit;
}

$user_id = $_SESSION['id'];

// Fetch all addresses for the logged-in user
$all_address = $obj->custom_get('addresses', " WHERE user_id = $user_id", "fetchAll");

if ($all_address) {
    foreach ($all_address as $address) {
        ?>
        <li class="list-group-item mb-4">
            <div class="fw-bold">
                <p class="text-uppercase">
                    <?= htmlspecialchars($address['first_name']) . ' ' . htmlspecialchars($address['last_name']); ?>
                </p>
            </div>
            <div>
                <p>
                    <strong>Address - </strong>
                    <?= htmlspecialchars($address['address_one']) . ', ' . htmlspecialchars($address['address_two']); ?>
                    <br>
                    <?= htmlspecialchars($address['city_name']) . ', ' . htmlspecialchars($address['state_name']) . ', ' . htmlspecialchars($address['country_name']); ?>
                </p>
                <p>
                    <strong>Phone - </strong><?= htmlspecialchars($address['phone_number']); ?>
                </p>
                <p>
                    <strong>Email - </strong><?= htmlspecialchars($address['email']); ?>
                </p>
            </div>
        </li>
        <?php
    }
} else {
    echo '<li class="list-group-item">No addresses found.</li>';
}
?>
