<?php

require_once '../../class/crud.php';
$obj = new Crud();

// uplooad the uploaded and store into database
if ($_POST['form_type'] == 'save') {

    $allowed_types = ['jpg', 'png', 'jpeg'];
    $max_file_size = 2 * 1024 * 1024; // 2MB
    $result = [];
    // check if the file is uploaded without any errors
    if ($_FILES['product_thumbnail']['error'] == UPLOAD_ERR_OK) {

        $file_name = $_FILES['product_thumbnail']['name'];

        $tmp_file_name = $_FILES['product_thumbnail']['tmp_name'];
        $file_size = $_FILES['product_thumbnail']['size'];

        $file_extension_name = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));

        if (!in_array($file_extension_name, $allowed_types)) {
            $result = [
                'status' => 'error',
                'message' => 'Invalid file extension ' . $file_extension_name
            ];
        } elseif ($file_size > $max_file_size) {
            $result = [
                'status' => 'error',
                'message' => 'Invalid file size. Max file size is - 2MB'
            ];
        } else {
            $new_file_name = date("Ymdhis") . '.' . $file_extension_name;

            $target_dir = '../../uploads/products';

            $target_file = $target_dir . '/' . $new_file_name;

            move_uploaded_file($tmp_file_name, $target_file);

            $product_data = [
                'product_title' => $_POST['product_title'],
                'category_id' => $_POST['category_name'],
                'brand_id' => $_POST['brand_name'],
                'regular_price' => $_POST['regular_price'],
                'selling_price' => $_POST['selling_price'],
                'short_description' => $_POST['short_description'],
                'long_description' => $_POST['long_description'],
                'status' => 1,
                'created_at' => date("Y-m-d H:i:s") // 2023-11-21 11:36:00
            ];
            if ($file_name) {
                $product_data['product_thumbnail'] = $new_file_name;
            }

            if ($obj->insert('products', $product_data)) {
                $result['status'] = 1;
            } else {
                $result['status'] = 0;
            }
        }
    } else {
        $result = [
            'status' => 'error',
            'message' => 'Error uploading file'
        ];
    }

    echo json_encode($result);
}
if ($_POST['form_type'] == 'fetch_product') {

    $product_id = $_POST['product_id'];

    $fetch_product = $obj->custom_get("products", " WHERE product_id = $product_id", 'fetch');

    echo json_encode($fetch_product);
}

if ($_POST['form_type'] == 'update_product') {

    if ($_FILES['product_thumbnail']['name'] != '') {

        $allowed_types = ['jpg', 'png', 'jpeg'];
        $max_file_size = 2 * 1024 * 1024; // 2MB
        $result = [];
        // check if the file is uploaded without any errors
        if ($_FILES['product_thumbnail']['error'] == UPLOAD_ERR_OK) {

            $file_name = $_FILES['product_thumbnail']['name'];

            $tmp_file_name = $_FILES['product_thumbnail']['tmp_name'];
            $file_size = $_FILES['product_thumbnail']['size'];

            $file_extension_name = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));

            if (!in_array($file_extension_name, $allowed_types)) {
                $result = [
                    'status' => 'error',
                    'message' => 'Invalid file extension ' . $file_extension_name
                ];
            } elseif ($file_size > $max_file_size) {
                $result = [
                    'status' => 'error',
                    'message' => 'Invalid file size. Max file size is - 2MB'
                ];
            } else {
                $new_file_name = date("Ymdhis") . '.' . $file_extension_name;

                $target_dir = '../../uploads/products';

                $target_file = $target_dir . '/' . $new_file_name;

                move_uploaded_file($tmp_file_name, $target_file);

            }
        } else {
            $result = [
                'status' => 'error',
                'message' => 'Error uploading file'
            ];
        }

    } else {
        $file_name = '';
    }
    // here is the process of updating our product
    $product_data = [
        'product_title' => $_POST['product_title'],
        'category_id' => $_POST['category_name'],
        'brand_id' => $_POST['brand_name'],
        'regular_price' => $_POST['regular_price'],
        'selling_price' => $_POST['selling_price'],
        'short_description' => $_POST['short_description'],
        'long_description' => $_POST['long_description'],
        'status' => 1,
        'created_at' => date("Y-m-d H:i:s") // 2023-11-21 11:36:00
    ];
    if ($file_name) {
        $product_data['product_thumbnail'] = $new_file_name;
    }
    $product_id = $_POST['product_id'];

    if ($update_query = $obj->update("products",$product_data,"WHERE product_id = '$product_id'")) {
        $result['status'] = 1;
    } else {
        $result['status'] = 0;
    }


    echo json_encode($result);
}

if($_POST['form_type'] == 'delete_product'){
    $product_id = $_POST['product_id'];
    $delete_product = $obj->delete("products"," where product_id = '$product_id'");

    if($delete_product){
        $result =  [
            'status' => 200,
            'message' => "Product deleted successfully"
        ];
    } else {
        $result =  [
            'status' => 401,
            'message' => "Something went wrong"
        ];
    }

    echo json_encode($result);
}
