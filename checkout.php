<?php
// include required things
include 'includes/header.php';
include 'includes/navbar.php';

require_once 'class/Crud.php';

$obj = new Crud();

?>
<div class="card">
    <div class="card-body">
        <h1 class="text-center py-5">Checkout</h1>

        <!-- container start -->
        <div class="container">
            <?php
            if (!isset($_SESSION['loggedIn'])) { ?>

                <h1 class="text-center pt-5 pb-3">You are not logged In.</h1>
                <div class="d-flex justify-content-center">
                    <a href="login.php" class="nav-link">Click here for login</a>
                </div>

            <?php } else {
                $user_id = $_SESSION['id'];

                // fetch cart items with their product name and details
                $cartQuery = "SELECT cart.id as cart_id, products.product_title, products.selling_price, products.regular_price, products.product_thumbnail, cart.quantity  FROM `cart` 
            
            LEFT JOIN products ON products.product_id = cart.product_id
            WHERE user_id = '$user_id'";


                $cartItems = $obj->query($cartQuery);

                ?>
                <div class="row">
                    <div class="col-md-7">
                        <a href="add_address.php" class="btn btn-warning">Add Address</a>
                    </div>

                    <div class="col-md-5 p-3" style="background: #f5f5f5;">
                        <!-- added cart items information -->
                        <h3>Total Items</h3>
                        <hr>

                        <ul class="list-group list-group-flush">
                            <?php
                            $total_final_price = 0;
                            foreach ($cartItems as $cartItem):

                                $total_price = $cartItem['selling_price'] * $cartItem['quantity'];
                                ?>
                                <li class="list-group-item d-flex align-items-center">
                                    <div>
                                        <img src="<?= 'uploads/products/' . $cartItem['product_thumbnail']; ?>"
                                            height="60px">
                                    </div>
                                    <!-- second flex box -->
                                    <div class="flex-grow-1">
                                        <p><?= $cartItem['product_title']; ?>
                                        <br>
                                        <?= number_format($cartItem['selling_price'], 2); ?> X <?= $cartItem['quantity']; ?></p>
                                    </div>

                                    <div class="fw-bold">
                                        <?= number_format($total_price, 2); ?>
                                    </div>
                                </li>
                            <?php 
                            $total_final_price += $total_price;
                        endforeach; ?>
                        </ul>

                        <hr>
                        <div class="d-flex justify-content-between">
                            <span class="h4">Total Final Price:</span>
                            <span class="total_final_price h4"><?= number_format($total_final_price, 2); ?></span>
                        </div>

                        <hr>

                        <p class="fw-bold">Payment Method</p>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="payment_method" id="cod" checked>
                            <label class="form-check-label" for="cod">
                                Cash On Delivery
                            </label>
                        </div>

                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="payment_method" id="stripe">
                            <label class="form-check-label" for="stripe">
                                Stripe
                            </label>
                        </div>

                        <hr>
                        <button class="btn btn-success btn-lg w-100">Place Order</button>

                    </div>
                </div>
            </div>

        <?php } ?>
    </div>
</div>

<?php
include 'includes/footer.php';
?>